# Another GPIO gem for Raspberry Pi

## But why ?

Because kernel 5.11 introduced new access for the chip and the old ways will fail.

See [this article from 2021](https://waldorf.waveform.org.uk/2021/the-pins-they-are-a-changin.html)

## Permissions

Please note that lib require read/write access to the devices `/dev/gpio*`. As of Ubuntu 21.10 these files are root.root and mode 600, so change them to mode 660 and set the group of your choice. I use the `i2c` group.

### Example

```ruby
require 'ruby-lgpio'

chip = LGPIO::Chip.new(0)
led  = Chip.new(26, LGPIO::OUTPUT)

led.high
sleep 1
led.low
sleep 1
```

## License

Copyleft 2022 - Nick Townsend - GNU GPLv3

