require 'bundler/gem_tasks'
require 'rake/clean'
require 'rake/testtask'
require 'rspec/core/rake_task'
require 'yard'

YARD::Rake::YardocTask.new do |t|
 t.files   = ['lib/*.rb']
 t.stats_options = ['--list-undoc']
end

RSpec::Core::RakeTask.new(:spec)

CLEAN << %w{
    ext/lgpio/Makefile
    ext/lgpio/ruby-lgpio.o
    ext/lgpio/mkmf.log
}

CLOBBER << %w{
    ext/lgpio/ruby-lgpio.so
    ext/lgpio/ruby-lgpio.bundle
    pkg/
}

# Run the benchmarks with the in-tree gem
#
desc "Run the benchmarks using the tree"
task :benchmark do
  ruby "-Ilib:ext/lgpio:#{$:.join(':')} test/benchmark.rb"
end

# Now we want to test with the in-tree gem, but Bundler doesn't support binary gems
# with the :path syntax, so we have to do it ourselves.
#
Rake::TestTask.new('test:unit') do |t|
  t.pattern = 'test/test*.rb'
  t.libs << 'ext/lgpio'
  t.libs << 'lib'
end

Rake::TestTask.new('test:spec') do |t|
  t.test_files = ['test/spec.rb']
  t.libs << 'ext/lgpio'
  t.libs << 'lib'
end

# Now although we have put the gem in the Gemfile with a :path
# bundler won't actually compile the extensions, so make it happen...
#
desc "Compile the C extensions so we can use in-tree"
task :compile do
  cd "ext/lgpio" do
    ruby "extconf.rb"
    sh "make"
  end
end
task 'test:unit' => :compile
task 'test:spec' => :compile

task :test => ['test:unit', 'test:spec']
task :default => :test

# vim: ft=ruby sts=2 sw=2 ts=8
