require "mkmf"

LIBDIR      = RbConfig::CONFIG['libdir']
INCLUDEDIR  = RbConfig::CONFIG['includedir']

HEADER_DIRS = [
  # First search /opt/local for macports
  '/opt/local/include',

  # Then search /usr/local for people that installed from source
  '/usr/local/include',

  # Check the ruby install locations
  INCLUDEDIR,

  # Finally fall back to /usr
  '/usr/include',
]

LIB_DIRS = [
  # First search /opt/local for macports
  '/opt/local/lib',

  # Then search /usr/local for people that installed from source
  '/usr/local/lib',

  # Check the ruby install locations
  LIBDIR,

  # Finally fall back to /usr
  '/usr/lib',
]

dir_config('lgpio', HEADER_DIRS, LIB_DIRS)

unless find_header('lgpio.h')
  abort "lgpio.h is missing.  please install liblgpio-dev"
end

unless find_library('lgpio', 'liblgpio')
  abort "liblgpio is missing.  please install liblgpio1"
end

cflags = %w{ -std=c99 }
cflags += %w{ -Wall -Wextra -O0 -g } if ENV['DEBUG']

with_cflags(cflags.join(' ')) do
  create_makefile("ruby-lgpio") do |conf|
    if RUBY_PLATFORM =~ /darwin/
      conf.map{|l| l.gsub(/^ARCH_FLAG.*/, 'ARCH_FLAG = -arch x86_64') }
    else
      conf
    end
  end
end
