/* Use the lgpio C-api to create an idiomatic Ruby API 
 * by Nick Townsend 2022
 */
#include <ruby.h>

static VALUE mLGPIO;

void Init_lgpio()
{
  mLGPIO = rb_define_module("LGPIO");
  Init_lgpio_chip();
}
