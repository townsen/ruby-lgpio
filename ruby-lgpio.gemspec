lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'lgpio/version'

Gem::Specification.new do |s|
  s.name        = 'lgpio'
  s.version     = LGPIO::VERSION
  s.date        = Time.now.utc.strftime("%Y-%m-%d")
  s.summary     = "GPIO module for Raspberry Pi using Ubuntu 21.04+"
  s.description = "LGPIO uses the new 5.11+ kernel interface to interface to GPIO"
  s.authors     = ["Nick Townsend"]
  s.email       = 'nick.townsen@mac.com'
  s.files       = `git ls-files`.split("\n")
  s.test_files  = s.files.grep(%r{^(test|spec)/})
  s.homepage    = 'https://gitlab.com/townsen/ruby-lgpio'
  s.metadata    = { "source_code_uri" => "https://github.com/townsen/ruby-lgpio" }
  s.license     = 'GPL-3.0'
  s.add_development_dependency 'rspec', '~> 3.8'
  s.add_development_dependency 'minitest'
  s.add_development_dependency 'rake'
  s.add_development_dependency 'yard', '~> 0.9'
end

