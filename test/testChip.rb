require 'lgpio'
require 'test/unit'
module LGPIO
class TestChip < Test::Unit::TestCase
  def test_length
    chip = LGPIO::Chip.new 0
    assert_greater_than 0, chip.lines
  end
  def test_type_error
    assert_raises(TypeError) do
      LGPIO::String.new Object.new
    end
  end
end
end
